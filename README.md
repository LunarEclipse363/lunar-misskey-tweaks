# Lunar Misskey Tweaks

## General
*These are intended to work in many misskey derivatives*

### Setting a different font
Here I am using jsdelivr because it lets you load files from any arbitrary github repository.
If you have the font installed on your computer, you can skip loading it from web.
```css
/* Load Atkinson Hyperlegible from a CDN */
@font-face {
  font-family: 'Atkinson Hyperlegible';
  src: url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/webfonts/AtkinsonHyperlegible-Regular.woff2') format('woff2'), /* Modern Browsers */
       url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/ttf/AtkinsonHyperlegible-Regular.ttf')  format('truetype'); /* Safari, Android, iOS */
       font-weight: normal;
       font-style: normal;
}
@font-face {
  font-family: 'Atkinson Hyperlegible';
  src: url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/webfonts/AtkinsonHyperlegible-Italic.woff2') format('woff2'),
       url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/ttf/AtkinsonHyperlegible-Italic.ttf')  format('truetype');
       font-weight: normal;
       font-style: italic;
}
@font-face {
  font-family: 'Atkinson Hyperlegible';
  src: url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/webfonts/AtkinsonHyperlegible-Bold.woff2') format('woff2'),
       url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/ttf/AtkinsonHyperlegible-Bold.ttf')  format('truetype');
       font-weight: bold;
       font-style: normal;
}
@font-face {
  font-family: 'Atkinson Hyperlegible';
  src: url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/webfonts/AtkinsonHyperlegible-BoldItalic.woff2') format('woff2'),
       url('https://cdn.jsdelivr.net/gh/googlefonts/atkinson-hyperlegible/fonts/ttf/AtkinsonHyperlegible-BoldItalic.ttf')  format('truetype');
       font-weight: bold;
       font-style: italic;
}

/* Set the main font */
html {
    font-family: "Atkinson Hyperlegible", sans-serif;
}
```

And for the font used in code blocks (font loading code skipped here):
```css
/* Set the font used in code blocks */
pre[class*="language-"], code[class*="language-"]{
  background: white !important;
  color: black !important;
  font-family: Fantasque Sans Mono, monospace !important;
  font-size: 12pt !important;
  text-shadow: none !important;
}
```

### Stopping the notification dots from blinking
They are extremely distracting for me.
```css
/* Stop the notification dots from blinking */
.indicator {
  animation: none !important;
}
```

### Show a red border around images with no alt text
```css
/* Show red border on images without alt text */
div.image img:not([alt]),
div.image img[alt=""] {
  border: 2px solid orangered;
  box-sizing: border-box;
  border-radius: var(--radius);
}
```

### Removing Specific Emojis
This snippet lets you stop any emote from being displayed at all.  
Particularly useful with blinking GIFs.  
```css
/* Completely stop emoji with the given names from being displayed */
.mk-emoji[title=":first_emoji:"], .mk-emoji[title=":second_emoji:"] {
  display: none !important;
}
```

### Improved Light Theme
For whatever reason, light theme currently has dark code blocks by default.  
This will just not do for me.
```css
/* Make code blocks light theme */
pre[class*="language-"], code[class*="language-"]{
  background: white !important;
  color: black !important;
  font-family: Fantasque Sans Mono, monospace !important;
  font-size: 12pt !important;
  text-shadow: none !important;
}
```

### Less annoying hashtags
*[Credit to Volpeon]*
```cs
:root {
    --hashtag: color-mix(in lab, var(--link) 40%, var(--fg)) !important;
}
```

## Software-specific
### Sharkey
#### Enlarge "Show Post" Button
```css
/* Enlarge content warning opening button - intended for the mobile version */
article > div > div[style="container-type: inline-size;"] > p > button {
  height: 3em;
  width: 100%;
  font-size: 10pt;
}
```

## License
<p xmlns:cc="http://creativecommons.org/ns#" >This work by <span property="cc:attributionName">LunarEclipse</span> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>
